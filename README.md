# List Style Type

## Overview

This module provides a field that can be added to entities, which allows editors to control the list-style-types for the ordered lists in the content, on a page-by-page basis. Editors can use this field to affect nested lists to as deep a level as they'd like.

## Use case example

This module is intended for the (very specific) use-case where site editors need an easy way to control the list-style-types for all of the ordered lists appearing on one page.

For example, if the lists on one page should look like this...

```
A. Parent item
    1. Child item
```

...while the lists on another page should look like this...

```
I. Parent item
    a. Child item
```

...and the editors would like an easy way to control them all, apart from manually tweaking the HTML in a WYSIWYG...

...then this module might be useful.

## WHen not to use

If you can unilaterally decide on a multi-level list-style-type pattern to be applied across the site, definitely don't use this module. Simply put your pattern in normal custom CSS. For example:

```
ol { list-style-type: upper-alpha; }
ol ol { list-style-type: lower-alpha; }
ol ol ol { list-style-type: decimal; }
etc...
```

This module is intended for the case where such as decision is not possible.

Also, if the editors are willing to manually tweak the list-style-types on a list-by-list basis, then you probably don't need this module. Instead, install a WYSIWYG plugin like [liststyle for CKEditor](https://ckeditor.com/cke4/addon/liststyle). This module is intended for the case where editors are not willing to tweak the list-style-types on a list-by-list basis, and instead want to be able to control them on a page-by-page basis.

## Dependencies

This module depends on the [Multiple Selects module](https://www.drupal.org/project/multiple_selects).

## Installation

Download and enable the module as normal.

## Usage

Add a new field to an entity bundle, and choose the type of "List Style Type". In the field settings, optionally choose allowed values and/or a selector prefix.

Set the field to display as normal, but hide the Label, and use the "Inline CSS" formatter.